# Change Log
All notable changes to this project will be documented in this file.

## 0.0.2 2015 - 01 - 26
- Engine Application Controller Derives from Parent Application Controller
- Bootstrap Classes added on pages#show


## 0.0.1 2015 - 01 - 16
Primary Release