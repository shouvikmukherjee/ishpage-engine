module Ishpage
  class Engine < ::Rails::Engine
    isolate_namespace Ishpage
  end

  require 'devise'
  require 'cancan'
  require 'friendly_id'
end
