$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ishpage/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ishpage"
  s.version     = Ishpage::VERSION
  s.authors     = ["Shouvik Mukherjee"]
  s.email       = ["contact@ishouvik.com"]
  s.homepage    = "http://ishouvik.com"
  s.summary     = "Dynamically manage pages"
  s.description = "Dynamically manage pages."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0"
  s.add_dependency "devise"
  s.add_dependency "cancan"
  s.add_dependency "friendly_id"

  s.add_development_dependency "sqlite3"
end
