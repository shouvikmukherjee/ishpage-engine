class CreateIshpagePages < ActiveRecord::Migration
  def change
    create_table :ishpage_pages do |t|
      t.string :title
      t.string :slug
      t.text :content

      t.timestamps null: false
    end
    add_index :ishpage_pages, :slug
  end
end
