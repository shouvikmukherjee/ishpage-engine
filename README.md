# iShpage Rails Engine
This is a simple ROR Engine for dynamically managing pages on ROR apps.

## Dependencies
-	Devise: https://github.com/plataformatec/devise
-	Cancan: https://github.com/ryanb/cancan
-	Friendly Id: https://github.com/norman/friendly_id

## Installation
-	Install the gem
	```
	gem 'ishpage', :path => 'git@bitbucket.org:shouvikmukherjee/ishpage-engine.git'
	```

-	Copy Migrations
	```
	rake ishpage:install:migrations
	```

-	Run Migrations
	```
	rake db:migrate
	```

-	Mount the Routes config
	```
	mount Ishpage::Engine => "/pages"
	```